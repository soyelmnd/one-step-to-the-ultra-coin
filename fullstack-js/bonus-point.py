# " Hint: make this as close to production-ready source code as you can!
#   Bonus points for telling us what this does in plain terms:
#   ''.join(itertools.chain(*zip(s[-2::-2], s[::-2]))) "
#                                                  - @mbogosian from Veritaseum

# Lol, such a really nice way of easter-eggs inspired challenge, I like it (y)
# In fact, Python hasn't become my stack yet, hence I'll tryna solve it with
# a big help from Google.

import itertools

s = 'Veritaseum' # suppose we have this string


s[-2::-2]
# Slice the `s` string
# * start from -2 (2-to-last char, which is `u`)
# * step of -2 (step back, 2 each)
# * end when out-of-range
#
# Like this (from the engine POV)
#   u
#   us
#   ust
#   ustr
#   ustrV
#
# @see http://pythoncentral.io/cutting-and-slicing-strings-in-python/


s[::-2]
# Similar, step back 2 each, from the end. Step-by-step, like this
#   m
#   me
#   mea
#   meai
#   meaie


zip(s[2::-2], s[::-2])
# Pack iterators accordingly, with the smallest number of element
# With this sample string, we have those 2 sliced strings with 5 elements,
# so all the characters will be mapped into pairs, no lonely char.
# Cool.
#
# Ok, so we'll have this snippet return something like
#   [
#     ('u', 'm'),
#     ('s', 'e'),
#     ('t', 'a'),
#     ('r', 'i'),
#     ('V', 'e')
#   ]
#
# @see https://docs.python.org/3/library/functions.html#zip


itertools.chain(*zip(s[2::-2], s[::-2]))
# The `splat` asterisk will make this snippet equal to
#   iterators.chain(
#     ('u', 'm'),
#     ('s', 'e'),
#     ('t', 'a'),
#     ('r', 'i'),
#     ('V', 'e')
#   )
#
# @see http://stackoverflow.com/questions/5239856/x
#
# And then the `itertools.chain` will concat all those tuples,
# the result will then become
#   ['u', 'm', 's', 'e', 't', 'a', 'r', 'i', 'V', 'e']
#
# @see https://docs.python.org/2/library/itertools.html#itertools.chain


''.join(itertools.chain(*zip(s[-2::-2], s[::-2])))
# Finally, stick those chars (join with empty delimiter), we'll have
#   'umsetariVe'


# Lol, I've just tested it via Python 3, it does return 'umsetariVe'
# Thanks in advanced for helping me learn more about Python the interesting
# way, I appreciate a lot indeed (y)
print(
    "s = '" + s + "'\n"
    + "''.join(itertools.chain(*zip(s[-2::-2], s[::-2]))) # should be '"
    + ''.join(itertools.chain(*zip(s[-2::-2], s[::-2])))
    + "'"
)
