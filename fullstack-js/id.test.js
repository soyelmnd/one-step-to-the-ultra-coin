describe('id', function() {
    it('should have NAMESPACE and NAMESPACE.resource()', function() {
        expect(NAMESPACE).toBeDefined();
        expect(typeof NAMESPACE.resource()).toBe('object');
    });


    it('should have a working life-cycle', function() {
        // first, create an object
        //   it should not be `closed` and should have the right id
        var obj = NAMESPACE.resource('the-first-id');
        expect(obj.getId()).toBe('the-first-id');
        expect(obj.closed).toBeFalsy();
        expect(obj.getExpensiveResource().value).toBe(
            "I'm a very expensive resource associated with ID the-first-id"
        );

        // then close it
        //   should clean everything
        obj.close();
        expect(obj.closed).toBeTruthy();
        expect(obj.getExpensiveResource()).toBe(null);
    });


    it('should have a working cache', function() {
        // Create a spy for the giantProcess
        //   (which is really a **giant** process)
        //   to count the number of invocation times
        giantProcess = jasmine.createSpy('giantProcess').and.callFake(giantProcess);


        // Create the first object
        //   grab the expensive resource for the first time,
        //   then grab it again, should not touch that `giantProcess` again
        var obj = NAMESPACE.resource('the-first-id');

        obj.getExpensiveResource();
        expect(giantProcess.calls.count()).toBe(1);
        expect(obj.getExpensiveResource().value).toBe(
            "I'm a very expensive resource associated with ID the-first-id"
        );

        obj.getExpensiveResource();
        expect(giantProcess.calls.count()).toBe(1);
        expect(obj.getExpensiveResource().value).toBe(
            "I'm a very expensive resource associated with ID the-first-id"
        );


        // Then another object with the same id,
        //   the `giantProcess` should not be called,
        //   also make sure the first object keeps intact
        var anotherObj = NAMESPACE.resource('the-first-id');
        anotherObj.getExpensiveResource();
        expect(giantProcess.calls.count()).toBe(1);
        expect(anotherObj.getExpensiveResource().value).toBe(
            "I'm a very expensive resource associated with ID the-first-id"
        );
        expect(obj.getExpensiveResource().value).toBe(
            "I'm a very expensive resource associated with ID the-first-id"
        );


        // Ok, another object with different id
        //   no cache, should invoke `giantProcess`
        anotherObj = NAMESPACE.resource('the-second-id');
        anotherObj.getExpensiveResource();
        expect(giantProcess.calls.count()).toBe(2);
        expect(anotherObj.getExpensiveResource().value).toBe(
            "I'm a very expensive resource associated with ID the-second-id"
        );
    });
})
