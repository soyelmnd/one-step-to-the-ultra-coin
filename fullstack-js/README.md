Quick start
===========

```bash
# render jsdoc to doc/index.html
jsdoc id.js -d doc/

# run tests
karma start
```
